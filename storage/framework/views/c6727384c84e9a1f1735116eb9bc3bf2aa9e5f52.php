<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/followers.css')); ?>" >



<?php $__env->startSection('content'); ?>

    <!-- Team -->
    <a href="/posts">home</a>
<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">People Following me</h5>
    </div>
</section>
<section id="team" class="pb-5">
<div class="container">    
        <div class="row">
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>        
            <div class="col-xs-12 col-md-4">          
                <div class="image-flip" >
                    <div class="mainflip flip-0">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="#" alt="card image"></p>
                                    <h1><?php echo e($user->name); ?></h1>
                                    <h4 class="card-title">Favorite food:<?php echo e($user->profile->favorite_food); ?></h4>
                                    <p class="card-text">Birth Day: <?php echo e($user->profile->birth_date); ?>.</p>
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">What's interesting about <?php echo e($user->name); ?></h4>
                                    <p class="card-text"><?php echo e($user->profile->bio); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
        </div>
    </div>
         
</section>
<?php $__env->stopSection(); ?>

            

<?php echo $__env->make('layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>