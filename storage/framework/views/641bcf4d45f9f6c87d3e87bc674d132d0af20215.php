<link href="css/profile" rel="stylesheet" type="text/css">
<div class="container">
 
	<div class="row">
		<div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
    	 <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2><?php echo e($user->profile->name); ?></h2>
                    <p><strong>About: </strong> <?php echo e($user->profile->name); ?> </p>
                    <p><strong>Birthdate: </strong> <?php echo e($user->profile->birth_date); ?> </p>
                    <p><strong>Favorite food: </strong> <?php echo e($user->profile->favorite_food); ?> </p>
                    <p><strong>Country: </strong> <?php echo e($user->profile->Country); ?> </p>
                
                </div>             
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle img-responsive">
                        <figcaption class="ratings">
                            <p>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a> 
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div>
                 
            <!-- <div class="col-xs-12 divider text-center">
                <div class="col-xs-12 col-sm-4 emphasis">
                    <h2><strong> 20,7K </strong></h2>                    
                    <p><small>Followers</small></p>
                    <button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Follow </button>
                </div> -->
                <!-- <div class="col-xs-12 col-sm-4 emphasis">
                    <h2><strong>245</strong></h2>                     -->
               
                    <a href="edit">Edit Profile</a> <br>
                    <a href="/posts/">Home</a>
                    </form> 
                </div>           
                 
               
                    </div>
                </div>
            </div>
    	 </div>                 
		</div>
	</div>
</div>

<?php echo $__env->make('layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>