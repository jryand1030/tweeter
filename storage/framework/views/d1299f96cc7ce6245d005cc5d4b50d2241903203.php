<link href="css/profile" rel="stylesheet" type="text/css">
<div class="container">
 
	<div class="row">
		<div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
    	 <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2><?php echo e($user->name); ?></h2>
                    <p><strong>About: </strong> <?php echo e($user->name); ?> </p>
                    <p><strong>Birthdate: </strong> <?php echo e($user->profile->birth_date); ?> </p>
                    <p><strong>Favorite food: </strong> <?php echo e($user->profile->favorite_food); ?> </p>
                    <p><strong>Country: </strong> <?php echo e($user->profile->Country); ?> </p>
                    <p><strong>BIO:</strong> <?php echo e($user->profile->bio); ?> </p>
          
               </div>      
            </div>
    
        <div>
            <a href="/follow/<?php echo e($user->id); ?>">Follow</a> <br />
            <?php if($user->id == Auth::id()): ?>
            <a href="/profile/edit">Edit Profile</a> <br>
            <?php endif; ?>
            <a href="/posts/">Home</a>
        </div>
        
        <div class="container">
            <div class="row text-center">
                <div class="col-12 text-center">
                    <h1 class="text-center"> Thoughts from a time in the past</h1>
                </div>
            </div>    
        <div class="card">
        <?php $__currentLoopData = $user->posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card-header">
            <?php echo e($post->created_at->diffForHumans()); ?>

          
            </div>
         
            <div class="card-body">
                <blockquote class="blockquote mb-0">
                <p> </p>
                <p> <?php echo e($post->title); ?></p>
                <p> <?php echo e($post->body); ?></p>    
                </blockquote>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
</div>          
            
               
                    </div>
                </div>
            </div>
    	 </div>                 
		</div>
	</div>
</div>

<?php echo $__env->make('layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>