<?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="text-right">
<a class="text-right" href="/logout">Logout</a>
</div>
<?php echo $__env->make('partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="jumbotron">
   
    <div class="container">

        <h1 class="text-center"> Welcome to Twitter <?php echo e(Auth::user()->name); ?> </h1>
        <p class="text-center"> Hear all the latest gossip that people are saying and stir the pot with your own explosive comments!</p>
        <a href="/posts/create" class="btn btn-primary">Make a new Tweet</a>
    </div>
</div>        


<div class="container-fluid">
<div class="row">
<?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
        <p class="text-left"><a href="/profile/<?php echo e($post->user->id); ?>"> <?php echo e($post->user->name); ?> </a><br>
        <?php echo e($post->created_at->diffForHumans()); ?>

        <h5 class="card-title"><?php echo e($post->title); ?></h5>
        <p class="card-text"><?php echo e($post->body); ?></p>
        <a href="/posts/<?php echo e($post->id); ?>/like">Likes:</a> <?php echo e(count($post->likes)); ?>

        
          <!-- edit only visible for user -->
        <?php if(Auth::id() == $post->user_id): ?>
        <a href="/posts/<?php echo e($post->id); ?>/edit" class="btn btn-sm btn-primary">Edit</a>
        <?php endif; ?>
        <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#commentmodal<?php echo e($post->id); ?>">Comments</button>
        
                                        <!-- Comments Modal -->
          <div class="modal fade" id="commentmodal<?php echo e($post->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Comments</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php $__currentLoopData = $post->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="card">
                      <div class="card-body">
                        <p class="text-center"> <?php echo e($comment->user->name); ?> <br>
                        <?php echo e($comment->created_at->diffForHumans()); ?>

                        <h5 class="card-title"><?php echo e($comment->body); ?></h5>
                       
                        <p class="card-text"></p>
                      </div>
                    </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                    <form class="form-horizontal" role="form" method="post"  action="/posts/<?php echo e($post->id); ?>/comments">
                                        <?php echo e(csrf_field()); ?>

                    <textarea class="form-control" name="body" rows='3' id='comment'></textarea>
                    
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add Comment</button>
                  </form>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>
          </div>                
      </div>
    </div>
  </div>    
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
  </div>





  

  
  

  
  
  
  
<?php echo $__env->make('layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>