<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/followers.css')); ?>" >
<a href="/posts">Home</a>
  


<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">People I Am Following</h5>
    </div>
</section>    
   
<div class="container">
    <section id="team" class="pb-5">        
        <div class="row">
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-xs-12 col-md-4">
                <div class="image-flip" >
                    <div class="mainflip flip-0">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                            
                                    <h1><?php echo e($user->name); ?></h1>
                                    <h4 class="card-title">Favorite food</h4>
                                        <p><?php echo e($user->profile->favorite_food); ?></p>
                                    <p class="card-text"> <?php echo e($user->profile->birth_date); ?></p>
                                    <a href="#" class="btn btn-primary btn-sm">Flip for more info</a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">What's interesting about <?php echo e($user->name); ?></h4>
                                    <p class="card-text"><?php echo e($user->profile->bio); ?></p>
                                        <form class="form-horizontal" role="form" method="post" action="/follow/<?php echo e($user); ?>"/>
                                    <input type="hidden" name="_method" value="delete"/>
                                        <?php echo e(csrf_field()); ?>

                                    <button type="submit" class="btn btn-primary">Unfollow</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </section>
</div>          





<?php echo $__env->make('layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>