-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 13, 2018 at 03:06 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crudburger`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 'do you have pink freezies?', '2018-08-13 14:01:38', '2018-08-13 14:01:38'),
(2, 4, 3, 'Hello John!', '2018-08-13 14:02:20', '2018-08-13 14:02:20'),
(3, 5, 3, 'This app is kinda nice actually.....', '2018-08-13 14:07:07', '2018-08-13 14:07:07'),
(4, 5, 4, 'You just wait Jane, this is nothing compared to what\'s coming!', '2018-08-13 14:07:48', '2018-08-13 14:07:48'),
(5, 6, 3, 'What do you know about Donald there my man?', '2018-08-13 14:13:47', '2018-08-13 14:13:47'),
(6, 6, 4, 'This app should be very good for business!', '2018-08-13 14:14:21', '2018-08-13 14:14:21'),
(7, 6, 2, 'Trade you for a chicken', '2018-08-13 14:14:50', '2018-08-13 14:14:50'),
(8, 6, 1, 'Kayla! Want to make some real money tonight? I have a chicken race going down tonight, lots of money to be made...I know you got the fastest chicken around....', '2018-08-13 14:16:36', '2018-08-13 14:16:36'),
(9, 5, 1, 'Talha, whats this all about?', '2018-08-13 14:19:08', '2018-08-13 14:19:08'),
(10, 5, 2, 'Yes! I need a whole box of blue ones!', '2018-08-13 14:20:10', '2018-08-13 14:20:10'),
(11, 7, 2, 'Red is the best! Ill have a couple!', '2018-08-13 14:25:00', '2018-08-13 14:25:00'),
(12, 7, 6, 'I would like to hear more about this. whose your source!', '2018-08-13 14:25:48', '2018-08-13 14:25:48'),
(13, 7, 1, 'following for no reason....', '2018-08-13 14:27:01', '2018-08-13 14:27:01'),
(14, 2, 1, 'Hey, I\'m interested too you know', '2018-08-13 14:28:15', '2018-08-13 14:28:15'),
(15, 2, 2, 'I\'ll put in an order. there going fast so the price may be steep', '2018-08-13 14:28:54', '2018-08-13 14:28:54'),
(16, 2, 5, 'sure sounds good!', '2018-08-13 14:32:19', '2018-08-13 14:32:19'),
(17, 2, 3, 'Thanks blackhat hacker dude, I think a freezie would make you fell much better', '2018-08-13 14:33:40', '2018-08-13 14:33:40'),
(18, 5, 7, 'Cool idea bro!', '2018-08-13 14:38:01', '2018-08-13 14:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `followee_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `followee_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-08-13 13:52:17', '2018-08-13 13:52:17'),
(2, 3, 1, '2018-08-13 13:56:52', '2018-08-13 13:56:52'),
(3, 4, 3, '2018-08-13 14:00:55', '2018-08-13 14:00:55'),
(4, 4, 2, '2018-08-13 14:00:59', '2018-08-13 14:00:59'),
(5, 5, 1, '2018-08-13 14:06:12', '2018-08-13 14:06:12'),
(6, 5, 3, '2018-08-13 14:06:15', '2018-08-13 14:06:15'),
(7, 5, 4, '2018-08-13 14:06:20', '2018-08-13 14:06:20'),
(8, 6, 2, '2018-08-13 14:11:57', '2018-08-13 14:11:57'),
(9, 6, 1, '2018-08-13 14:11:59', '2018-08-13 14:11:59'),
(10, 6, 3, '2018-08-13 14:12:02', '2018-08-13 14:12:02'),
(11, 6, 3, '2018-08-13 14:12:06', '2018-08-13 14:12:06'),
(12, 6, 4, '2018-08-13 14:12:11', '2018-08-13 14:12:11'),
(13, 6, 5, '2018-08-13 14:12:14', '2018-08-13 14:12:14'),
(14, 5, 2, '2018-08-13 14:19:33', '2018-08-13 14:19:33'),
(15, 7, 1, '2018-08-13 14:24:16', '2018-08-13 14:24:16'),
(16, 7, 2, '2018-08-13 14:24:20', '2018-08-13 14:24:20'),
(17, 7, 4, '2018-08-13 14:24:23', '2018-08-13 14:24:23'),
(18, 7, 5, '2018-08-13 14:24:27', '2018-08-13 14:24:27'),
(19, 7, 5, '2018-08-13 14:24:31', '2018-08-13 14:24:31'),
(20, 2, 3, '2018-08-13 14:29:47', '2018-08-13 14:29:47'),
(21, 2, 3, '2018-08-13 14:29:52', '2018-08-13 14:29:52'),
(22, 2, 7, '2018-08-13 14:30:35', '2018-08-13 14:30:35'),
(23, 2, 7, '2018-08-13 14:31:40', '2018-08-13 14:31:40'),
(24, 2, 6, '2018-08-13 14:31:54', '2018-08-13 14:31:54'),
(25, 5, 1, '2018-08-13 14:41:16', '2018-08-13 14:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-08-13 13:52:26', '2018-08-13 13:52:26'),
(2, 3, 3, '2018-08-13 13:56:34', '2018-08-13 13:56:34'),
(3, 4, 2, '2018-08-13 14:01:45', '2018-08-13 14:01:45'),
(4, 4, 3, '2018-08-13 14:02:29', '2018-08-13 14:02:29'),
(5, 5, 4, '2018-08-13 14:07:51', '2018-08-13 14:07:51'),
(6, 5, 1, '2018-08-13 14:07:55', '2018-08-13 14:07:55'),
(7, 6, 3, '2018-08-13 14:13:14', '2018-08-13 14:13:14'),
(8, 6, 4, '2018-08-13 14:14:01', '2018-08-13 14:14:01'),
(9, 7, 2, '2018-08-13 14:24:40', '2018-08-13 14:24:40'),
(10, 7, 4, '2018-08-13 14:25:15', '2018-08-13 14:25:15'),
(11, 7, 6, '2018-08-13 14:25:23', '2018-08-13 14:25:23'),
(12, 7, 1, '2018-08-13 14:26:17', '2018-08-13 14:26:17'),
(13, 2, 2, '2018-08-13 14:28:23', '2018-08-13 14:28:23'),
(14, 2, 5, '2018-08-13 14:32:22', '2018-08-13 14:32:22'),
(15, 2, 3, '2018-08-13 14:32:27', '2018-08-13 14:32:27'),
(16, 5, 6, '2018-08-13 14:37:26', '2018-08-13 14:37:26'),
(17, 5, 7, '2018-08-13 14:37:34', '2018-08-13 14:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(23, '2014_10_12_000000_create_users_table', 1),
(24, '2014_10_12_100000_create_password_resets_table', 1),
(25, '2018_07_26_202949_create_posts_table', 1),
(26, '2018_07_30_161547_create_comments_table', 1),
(27, '2018_08_08_151619_create_profiles_table', 1),
(28, '2018_08_09_171626_create_followers_table', 1),
(29, '2018_08_10_212040_create_likes_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, '1', 'mmmmm', 'I wonder what this app is all about.\r\n\r\nprobably mine is better', '2018-08-13 13:48:37', '2018-08-13 13:48:37'),
(2, '2', 'so hot right now', 'Any body want some freezees?', '2018-08-13 13:52:03', '2018-08-13 13:52:03'),
(3, '3', 'This app is lacking in quality', 'Amateurs, i have an app that can access top secret documents from Donald Trumps secret vault of secrets, and here he barely has a twitter app.', '2018-08-13 13:56:25', '2018-08-13 13:56:25'),
(4, '4', 'Hello new strange app', 'Wow i have never seen an app where you can post a message, and other people can comment before... this think will be huge!!', '2018-08-13 14:00:45', '2018-08-13 14:00:45'),
(5, '6', 'Great deal!', 'Anybody want to buy a chicken? I have three for sale....fastest chickens around.', '2018-08-13 14:13:02', '2018-08-13 14:13:02'),
(6, '5', 'What\'s going on?', 'Word on the street is that theres some kind of chicken race going on tonight?? anybody have an information on this?? I am not a cop, don\'t worry!', '2018-08-13 14:18:40', '2018-08-13 14:18:40'),
(7, '2', 'I have a new idea', 'I\'m going to sell underground freezies to the chicken racers.... there will gobble that shit right up!', '2018-08-13 14:35:11', '2018-08-13 14:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favorite_food` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `name`, `bio`, `birth_date`, `favorite_food`, `Country`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Like to walk her dogs while thinking about clever ways to take over the world.', 'sept', 'wine', 'canada', '2018-08-13 13:47:16', '2018-08-13 13:48:07'),
(2, 2, NULL, 'Likes to have random starring contests with random strangers on the street. People think he\'s so cool. Someday hopes to have freezee brand named after him.', 'Oct30', 'Pizza', 'Canada', '2018-08-13 13:49:06', '2018-08-13 13:51:40'),
(3, 3, NULL, 'sun\'s out gunn\'s out bitches. Has a passion for programming and will one day make a program that will make him filthy rich, like Taj.', 'Nov', 'spicy perogies', 'Canada', '2018-08-13 13:53:03', '2018-08-13 13:54:54'),
(4, 4, NULL, 'Coming from the beautiful country of Korea, she will someday make it big as a k-pop star and will be very rich and famous. Also her stealth and skills as a high end thief will allow her to steal the Mona Lisa..', 'Sept', 'Sushie', 'Korea', '2018-08-13 13:57:25', '2018-08-13 13:59:51'),
(5, 5, NULL, 'A famous rock climber from Colombia with a dark past. Rumor has it he has some high profile friends in Washington and taking web development is a cover for his real job of stealing trade secrets from the chicken farmers in Balzac....', 'Jan 1 1985', 'Tacos', 'Colombia', '2018-08-13 14:03:32', '2018-08-13 14:06:05'),
(6, 6, NULL, 'You wouldn\'t know it to see him, but this budding entrepreneur has a secret underground chicken racing ring where millions of dollars are made betting on chickens racing down stephen avenue during the night. So far he has eluded police...for now..', 'July', 'Bagels', 'Canada', '2018-08-13 14:08:40', '2018-08-13 14:11:50'),
(7, 7, NULL, 'Programmer by day, vigilante by night. Takes it upon himself to keep crime in it\'s place by helping the police on ride alongs during the night. He\'s hot on the trail as he\'s heard in the secret dark web chatroom that there is an illegal chicken racing ring going on during the night hours...', 'March', 'Pizza', 'Canada', '2018-08-13 14:22:01', '2018-08-13 14:24:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kayla', 'Kayla@Kayla.com', '$2y$10$LYQL9RNZA0iAYe3n.uuR9eYUslfOyBHMmnOMTUxz8SCdkT9Wsmx6m', 'jStLNcCNSctiojeSBD4MO0MYGqdZEHY07aokucJC8XiPRbNSQoIp5e9Xfw3E', '2018-08-13 13:47:16', '2018-08-13 13:47:16'),
(2, 'Ryan', 'Ryan@Ryan.com', '$2y$10$DaEOj.BcdDoTCzqqi8Axgez5Aushbi9oBSMfMOaO9.BQV.L34m9hy', 'EM6GRFHFUCq8J9VyVRwiNG5zaGjrSmA1Uec2u0xSFQtvQplBOUfvZtGi3vCO', '2018-08-13 13:49:06', '2018-08-13 13:49:06'),
(3, 'John', 'John@John.com', '$2y$10$xpsP/rsyjzsi8W36JDnmTuimvAYB8ovEiUXAcE.OwZiJXfOdXCBdS', '6iIgoWgCOk5xKvBj0bDYIwMoKa7XTTl9mTHKKIV4cLVeUJoC9d6G2oWJO03P', '2018-08-13 13:53:03', '2018-08-13 13:53:03'),
(4, 'jane', 'jane@jane.com', '$2y$10$Ljb5j/yFeQUV.zWkjORFfeeDcyDdoTdp2ZEljUf0uK6QOQv0ALno.', 'WiZCKNDVPTXS8W7OgKWF92QHTxB9hAX2yyVceqYH8L5yanbmEVzPJk8PE0Pr', '2018-08-13 13:57:25', '2018-08-13 13:57:25'),
(5, 'Wiliam', 'William@william.com', '$2y$10$792wEd9kJxW08vZRB7Q17OOZFbW36risKMF5io8fWGb9o8f9kTrgi', 'AA8Vg3ykBtwVmxqbaOBgrOvnUQ6ikF0WTKDMty8aWITRcZPjdcJAVONHFz01', '2018-08-13 14:03:32', '2018-08-13 14:03:32'),
(6, 'Talha', 'Talha@Talha.com', '$2y$10$eLitJ.zAfezLHtOBEz5vN.4bEjDWYqzLRPwm9C27SSe7eX7nT8l6.', 'o7QfLZZ1z4ehsCtqGZKDBp5f33u6Q5pTkSVEBVqZvQhrWV4wOsFlHsaQCRii', '2018-08-13 14:08:40', '2018-08-13 14:08:40'),
(7, 'James', 'James@james.com', '$2y$10$jUdMgCD2XjmiaO7QP/haNeCOVKkl3HWk6svaM6aRXuC5x4xkMe.1a', 'EC74foehZwtZOmP01IsUnx0n6KkgfnnPVAxhyw9uYABTwTK3DvuIKOz1dxQm', '2018-08-13 14:22:01', '2018-08-13 14:22:01');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
